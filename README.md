Complete redesign and rebuild of the ARC Assessment website. A live version is available at https://andrewwhitedesign.com/pog-universe/

Included are minified and expanded versions of the html and css files.